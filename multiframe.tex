\section{Tâches Multiframe}


\paragraph{Modèle de tâche}
Dans le modèle multiframe présenté par \cite{Mok} chaque tâche $\tau_i$ est décrite par le couple $<E_i,T_i>$ où $E$ est une liste de $N$ temps d’exécution ($C_i^0,C_i^1,\dots,C_i^{N-1}$) ($N\geq1$) et $T$ est le temps de minimum entre les dates de réveil de deux instances consécutives. La $i$-ème instances de $\tau$ à un temps d’exécution égale à $C^{(i~mod~N)}$, la deadline de chaque instance est $T$. Ce modèle ce rapproche du modèle de tâche sporadique présenté par \cite{LiuLay} mais où les temps d’exécution peuvent varier de manière periodique.

Dans le modèle multiframe généralisé présenté par \cite{Baruah1999} chaque tâche est décrite par $<E,D,T>$ où $E$, $D$ et $T$ sont des listes de tailles $N$ ($N\geq$1). Ainsi la $i$-ème instances d'un tache $\tau_k$ a une durée d’exécution $E_k^{(i~mod~N)}$, une deadline relative $D^{(i~mod~N)}$ et une séparation minimum avec la tâche suivante $T_k^{(i~mod~N)}$. En notant $a_k^i$ la date réveil d'une instance on a :
\begin{equation}
    \begin{cases}
    a_k^0 \geq 0\\
    a_k^{i+1} \geq a^i+T^{i~mod~N}
\end{cases}
\end{equation}

\subsection{Multiframe \cite{Mok}}

\subsubsection{Préconditions}
$\emptyset$

\subsubsection{Méthodes}
La méthode proposé par Mok et Chen est l'utilisation d'une borne similaire à celle proposée par Liu et Layland. Cette borne est définie par (\ref{eq:borneMok}) et porte sur le facteur d'utilisation maximum $U^m(S)$ définie, pour un système $S$ de $n$ tâches comportant chacune $N_i$ frames, par (\ref{eq:maxfact}).
\begin{equation}
    \label{eq:maxfact}
    U^m(S) = \sum_{i=1}^n\frac{max_{j=0}^{N_i-1}C_i^j}{T_i}
\end{equation}
% \begin{equation}
%     U^v(S) = \sum_{i=1}^n\frac{\sum_{j=0}^{N_i-1}C_i^j}{N_iT_i}
% \end{equation}
\begin{equation}
    r_i = \begin{cases}
        \frac{C_i^0}{C_i^1} \text{ si } N_i \geq 2\\
        1 \text{ si } N_i = 1
    \end{cases}
\end{equation}
\begin{equation}
    \label{eq:borneMok}
    \overline{U^m} = r\cdot n\cdot\left(\left(\frac{r+1}{r}\right)^{1/n}-1\right)
\end{equation}

\paragraph{Rq}Dans le cas de tâches périodiques on a $r=1$ et on retrouve exactement la borne de Liu et Layland.
\subsubsection{Qualité du résultat}
Comme la borne de Liu et Layland celle-ci est pessimiste, elle l'est d'autant plus si les tâches sont sporadiques et ont des périodes réels plus grande que celles théoriques.\\

Mok et Chen ont aussi montré que RM est optimal dans la catégorie des algorithmes statiques pour les tâches multiframes.

\subsection{Multiframe généralisé \cite{Baruah1999}}

\subsubsection{Préconditions}
\paragraph{l-MAD (localized Monotonic Absolute Deadlines) property} Les frames ne doivent pas avoir leur deadline après celle de la frame suivante : $\forall i , D_i < P_i + D_{i+1}$

\subsubsection{Méthodes}
La méthode est similaire à celle utilisée pour le cas des tâches periodiques avec date de réveil (Section~\ref{sec:dbf}). Il s'agit de calculer la demande processeur maximum et de vérifier quelle est toujours inférieur à $t$.\\

On définie $dbf(\tau,t)$ (demande bound fonction) comme étant le maximum de demande processeur dû à la tâche $\tau$ sur tout intervalle de duré $t$. Pour les intervalles contenant $N$ tâches au maximum on utilise l'algorithme $build-list$ qui permet de créer une liste de couples $(w,\Delta t)$ contenant la demande d’exécution maximal pour chaque séquence d'au plus $N$ instances et la taille de l'intervalle dans lequel cette demande d’exécution doit être réalisée.\\

\begin{leftbar}
    % \centering
    \underline{\textbf{\emph{\Large build-list}}}
\begin{algorithmic}
    \State $L\gets []$
    \For {$i=0\to N-1$}
        \For{ $j=0\to N-1$}
            \State $L\gets L + (e(R_i)+e(R_{i+1})+\dots+e(R_{i+j}), d(R_{i+j})-a(R_i))$
        \EndFor
    \EndFor
    \State Trier $L$ par taille d'intervalle croissante puis par demande processeur décroissante.
    \State Supprimer toutes les paires qui n'ont pas une demande processeur strictement plus grande que celles de toutes les paires précédente.
\end{algorithmic}
\end{leftbar}
Pour obtenir $dbf(t)$ il suffit de prendre, dans la liste finale la plus grande valeur de charge processeur parmi tous les couples dont la durée est inférieur à $t$.
Pour les séquences de plus de $N$ instances on peut les décomposer $q$ séquences de $N$ instances plus une séquence de $r$ instances (avec $0\leq r<N$ et $nb\_instance = q\cdot N +r$). On obtient donc la definition de la $dbf$ donné par (\ref{eq:dbfgmf}).
\begin{equation}
    \label{eq:dbfgmf}
    dbf(t) = \begin{cases}
        \max \{w | (w,\Delta t_w)\in build\_list(t)\text{ et } \Delta t_w\leq t \} \text{ si }t < P+D_{min}\\
        \left\lfloor\frac{t-D_{min}}{P}\right\rfloor \cdot E + dbf(D_{min}+(t-D_{min})\ mod\ P) \text{ si }t > P +D_{min}
    \end{cases}
\end{equation}
avec $D_{min} = \min\limits_{0\leq i\leq N-1} \{D^i\}$, $E=\sum\limits_{i=0}^{N-1}E^i$ et $P = \sum\limits_{i=0}^{N-1}P^i$.

\begin{leftbar}
    % \centering
    \underline{\textbf{\emph{\Large test de faisabilité}}}
\begin{algorithmic}
    \State Appliquer à chaque tâche l'algorithme \verb'build-list'
    \State Soit $Q$ une liste de triplets (temps, tâche, demande processeur)
    \State Pour chaque tâche $\tau_k$ ajouter dans $Q$ $(t_k,\tau_k,dbf(\tau_k,t_k))$ où $t_k$ est le plus petit temps pour lequel $dbf(\tau_k,t_k)>0$
    \State $S\gets 0$
    \Repeat
    \State $(t_0, \tau_0, d)\gets min(Q)$
    \State $S\gets S + d$
    \If{$S>t_0$}
    \State return "non-ordonnançable"
    \EndIf
    \State Soit $t'$ le plus petit temps tel que $t'>t_0$ et $dbf(\tau_0,t') > dbf(\tau_0,t_0)$
    \State Insérer $(t', \tau_0, dbf(\tau_0,t'))$ dans $Q$
    \Until
\end{algorithmic}
\end{leftbar}

\subsubsection{Qualité du résultat}

\subsection{Exemple}
\subsubsection{Exemple 1}
\label{sec:ex1gmf}

Soit $\tau <[1,2,5,1],[2,2,8,5],[3,2,3,4]>$ une tâche multiframe généralisée. Le tableau \ref{tab:exbuild} montre toutes les paires générées par l'algorithme \verb'build-list'. La liste des paires triée est : [(2,2), (1,2), (3,5), (1,5), (2,6), (6,8), (5,8), (7,9), (4,9), (8,10), (7,10), (9,11), (9,12), (9,13), (8,13), (9,17)]. Après la suppression des paires redondantes elle devient : [(2,2), (3,5), (6,8), (7,9), (8,10), (9,11)].

\begin{table}
    \centering
    \begin{tabular}{|c|cccc|}
        \hline
        & j=0 & j=1 & j=2 & j=3\\
    \hline
    i=0 & (1,2) & (3,5) & (8,13) & (9,13)\\
    i=1 & (2,2) & (7,10) & (8,10) & (9,11)\\
    i=2 & (5,8) & (6,8) & (7,9) & (9,12)\\
    i=3 & (1,5) & (2,6) & (4,9) & (9,17)\\
    \hline
    \end{tabular}
    \caption{Paires générer par build-list pour l’exemple \ref{sec:ex1gmf}}
    \label{tab:exbuild}
\end{table}

\begin{figure}
    \centering
    \input{schema/dbfex1.tex}
    \caption{dbft(t) pour l'exemple \ref{sec:ex1gmf}}
\end{figure}